# CloudWatch Migration (off)

## Dependencies

* `dateutils`
* `cloudwatch-dump`

## Process

### Generate date/time list

For example:

```bash
dateseq 2019-06-01T00:00:00 60m 2019-06-02T00:00:00
```

To format for `cloudwatch-dump`:

```bash
dateseq 2019-06-01T00:00:00 60m 2019-06-02T00:00:00 | sed -E -e 's/-|T|://g'
```

Start date visible in CloudWatch: 2019-05-01 10:00 UTC
End date chosen: 2020-06-01 00:00 UTC

`cloudwatch-dump` expects the time to only be in hours and minutes, so the seconds are trimmed from the results.

The final command should be:

```bash
dateseq 2019-05-01T10:00:00 60m 2020-06-02T00:00:00 | sed -E -e 's/-|T|://g' | cut -c -12 > daterange.txt
```

The resulting file will be used to seed the `cloudwatch-dump` tool, and should look something like:

```txt
20200601150000
20200601160000
20200601170000
20200601180000
20200601190000
20200601200000
20200601210000
20200601220000
20200601230000
20200602000000
```

### CloudWatch dump

The CloudWatch dump uses the `daterange.txt` file generated with `dateseq` to loop through each hour and grab the relevant metrics. While the `hiscores2cloudwatch.sh` script ran every 15 minutes, CloudWatch doesn't keep the information at that granularity for long enough to pull it all back.

The script `osrs-cloudwatch-dump.sh` runs `cloudwatch-dump` for each datetime, and exports the results to a file.

### Filtering the data

Each file returned by `cloudwatch-dump` will also contain other metrics that aren't required - metrics relating to EC2, DynamoDB etc.
