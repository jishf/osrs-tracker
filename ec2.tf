resource "aws_instance" "osrs-grafana" {
  ami               = "${var.osrs_ami}"
  instance_type     = "${var.osrs_type}"
  availability_zone = "${var.dublin}a"
  provider = "aws.dublin"

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 8
    delete_on_termination = true
  }

  vpc_security_group_ids      = ["${aws_security_group.osrs.id}"]
  associate_public_ip_address = "true"
  key_name                    = "${aws_key_pair.keypair.key_name}"
  subnet_id                   = "${aws_subnet.osrs-public.id}"
}
