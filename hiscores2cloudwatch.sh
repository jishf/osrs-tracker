#!/bin/bash

hiscores=https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws
get_hiscores=$(curl -s --data-urlencode "player=$1" $hiscores > /tmp/hiscores.csv)

counter=0
INPUT=/tmp/hiscores.csv
skills=( "Overall" "Attack" "Defence" "Strength" "Hitpoints" "Ranged" "Prayer" "Magic" "Cooking" "Woodcutting" "Fletching" "Fishing" "Firemaking" "Crafting" "Smithing" "Mining" "Herblore" "Agility" "Thieving" "Slayer" "Farming" "Runecrafting" "Hunter" "Construction")

OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

while read rank level experience
do
	if [ $counter -le 23 ]; then
	echo "${skills[$counter]} Rank : $rank"
    aws cloudwatch --profile osrs put-metric-data --metric-name Rank --namespace ${skills[$counter]} --value $rank --dimension username=$1
	echo "${skills[$counter]} Level : $level"
    aws cloudwatch --profile osrs put-metric-data --metric-name Level --namespace ${skills[$counter]} --value $level --dimension username=$1
	echo "${skills[$counter]} Experience : $experience"
    aws cloudwatch --profile osrs put-metric-data --metric-name Experience --namespace ${skills[$counter]} --value $experience --dimension username=$1
	fi
	let counter=counter+1
done < $INPUT
IFS=$OLDIFS

rm -f /tmp/hiscores.csv