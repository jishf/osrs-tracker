provider "aws" {
  region = "${var.dublin}"
  alias = "dublin"
}

terraform {
  backend "s3" {}
}
