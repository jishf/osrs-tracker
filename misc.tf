resource "aws_key_pair" "keypair" {
  key_name   = "osrs-tracker"
  public_key = "${var.key_pair}"
  provider = "aws.dublin"
}

resource "aws_key_pair" "keypair-hacktop" {
  key_name   = "osrs-tracker-hacktop"
  public_key = "${var.key_pair_hacktop}"
  provider = "aws.dublin"
}

/*
resource "aws_iam_role" "osrs-role" {
    name                = "osrs-role"
    path                = "/"
    assume_role_policy  = "${data.aws_iam_policy_document.osrs-policy.json}"
    provider = "aws.dublin"
}

data "aws_iam_policy_document" "osrs-policy" {
    provider = "aws.dublin"
    statement {
        actions = ["sts:AssumeRole"]

        principals {
            type        = "Service"
            identifiers = ["ec2.amazonaws.com"]
        }
    }
}

resource "aws_iam_role_policy_attachment" "osrs-role-attachment" {
    role       = "${aws_iam_role.ecs-instance-role.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
    provider = "aws.dublin"
}

resource "aws_iam_instance_profile" "osrs-profile" {
    name = "osrs-profile"
    path = "/"
    role = "${aws_iam_role.osrs-role-role.id}"
    provider = "aws.dublin"
    provisioner "local-exec" {
      command = "sleep 10"
    }
}
*/