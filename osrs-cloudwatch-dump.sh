#!/bin/bash
  
daterangefile=./daterange.txt
outputdir=./output
datecount=$(wc -l $daterangefile | sed -e 's/^[[:space:]]*//')

# basic checks

if test -f "$daterangefile"; then
    echo "$daterangefile exists, continuing."
else
    echo "$daterangefile not found, exiting."
    exit 1
fi

if test -d "$outputdir"; then
    echo "$outputdir exists, continuing."
else
    echo "$outputdir doesn't exist, creating."
    mkdir $outputdir
fi

# grab metrics from CloudWatch

while read date; do
        cloudwatch-dump --region eu-west-1 --time $date --interval 60 --period 60 >> $outputdir/$date.txt
done <$daterangefile

# final checks

outputfilecount=$(ls $outputdir | wc-l | sed -e 's/^[[:space:]]*//')

echo "Number of datetimes expected: $datecount."
echo "Number of datetimes expected: $outputfilecount."

./telegram.sh