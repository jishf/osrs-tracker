variable "dublin" {
  default = "eu-west-1"
}

variable "root_domain" {
  default = "dhcpsucks.scot"
}

variable "key_pair" {}

variable "key_pair_hacktop" {}

variable "osrs_ami" {
  default = "ami-07683a44e80cd32c5"
}

variable "osrs_type" {
  default = "t2.micro"
}

variable "dhcp_sucks_VPN" {
  default = "163.172.136.41/32"
}
