resource "aws_vpc" "osrs" {
  cidr_block           = "10.20.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  provider             = "aws.dublin"
}

resource "aws_vpc_dhcp_options" "osrs-dhcp" {
  domain_name_servers = ["AmazonProvidedDNS"]
  provider            = "aws.dublin"
}

resource "aws_vpc_dhcp_options_association" "osrs-dhcp" {
  vpc_id          = "${aws_vpc.osrs.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.osrs-dhcp.id}"
  provider        = "aws.dublin"
}

resource "aws_route_table" "osrs-route" {
  vpc_id   = "${aws_vpc.osrs.id}"
  provider = "aws.dublin"

  route = {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
}

resource "aws_route_table_association" "osrs-route" {
  subnet_id      = "${aws_subnet.osrs-public.id}"
  route_table_id = "${aws_route_table.osrs-route.id}"
  provider       = "aws.dublin"
}

resource "aws_internet_gateway" "igw" {
  vpc_id   = "${aws_vpc.osrs.id}"
  provider = "aws.dublin"
}

resource "aws_subnet" "osrs-public" {
  vpc_id            = "${aws_vpc.osrs.id}"
  cidr_block        = "10.20.32.0/19"
  availability_zone = "${var.dublin}a"
  provider          = "aws.dublin"
}

resource "aws_security_group" "osrs" {
  name     = "osrs"
  provider = "aws.dublin"

  description = "OSRS security group"
  vpc_id      = "${aws_vpc.osrs.id}"

  ingress {
    description = "allow SSH from VPN(s)"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.dhcp_sucks_VPN}"]
  }

  ingress {
    description = "allow HTTPS from le monde"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow HTTPS from le monde"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow ICMP from le monde"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
